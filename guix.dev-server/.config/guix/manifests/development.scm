;; -*- mode: guix-scheme-*-

(specifications->manifest
 '(
     "neovim"
     "tree-sitter-cli"
     "python-pynvim"
     "node"
     "zsh"
     "zsh-autosuggestions"
     "zsh-syntax-highlighting"
     "zsh-completions"
     "starship"
     "zoxide"
     ;; "fzy"
     "fzf"
     "go-github-com-junegunn-fzf"
     "ripgrep"
     "git"
  ))
