  " luafile ~/.config/nvim/plugin-config/catppuccin.lua
  luafile ~/.config/nvim/plugin-config/file-tree.lua
  luafile ~/.config/nvim/plugin-config/treesitter-config.lua
  luafile ~/.config/nvim/plugin-config/which-key-config.lua
  luafile ~/.config/nvim/plugin-config/telescope.lua
  luafile ~/.config/nvim/plugin-config/dap.lua
  luafile ~/.config/nvim/plugin-config/todo-comments.lua
  source ~/.config/nvim/plugin-config/markdown-preview.vim
  luafile ~/.config/nvim/plugin-config/gitsigns.lua
  source ~/.config/nvim/plugin-config/codi.vim
  source ~/.config/nvim/plugin-config/undotree.vim
  luafile ~/.config/nvim/plugin-config/router.lua
  source ~/.config/nvim/plugin-config/vista.vim
  luafile ~/.config/nvim/plugin-config/fidget.lua
  luafile ~/.config/nvim/plugin-config/tabnine.lua
