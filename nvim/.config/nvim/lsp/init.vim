  " LSP: https://github.com/neovim/nvim-lspconfig/blob/master/CONFIG.md
  source ~/.config/nvim/lsp/lsp-config.vim
  luafile ~/.config/nvim/lsp/lsp-config.lua
  luafile ~/.config/nvim/lsp/cmp-config.lua
  " luafile ~/.config/nvim/lsp/lspsaga-config.lua
  " Override lsp keymap
  luafile ~/.config/nvim/lsp/javascript-ls.lua
  luafile ~/.config/nvim/lsp/php-ls.lua
  luafile ~/.config/nvim/lsp/python-ls.lua
  luafile ~/.config/nvim/lsp/latex-ls.lua
  luafile ~/.config/nvim/lsp/go-ls.lua
  luafile ~/.config/nvim/lsp/css-ls.lua
  luafile ~/.config/nvim/lsp/svelte-ls.lua
  luafile ~/.config/nvim/lsp/ccls-ls.lua
  luafile ~/.config/nvim/lsp/arduino-ls.lua
  luafile ~/.config/nvim/lsp/docker-ls.lua
  luafile ~/.config/nvim/lsp/yaml-ls.lua
  luafile ~/.config/nvim/lsp/lua-ls.lua
  luafile ~/.config/nvim/lsp/bash-ls.lua
  luafile ~/.config/nvim/lsp/rust-ls.lua
  luafile ~/.config/nvim/lsp/eslint-ls.lua
