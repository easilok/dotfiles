;; -*- mode: guix-scheme-*-

(specifications->manifest
 '(
     ;; devtools
     "clojure"
     "clojure-lsp-bin"
     "cmake"
     "make"
     ;; "go"
     "guile"
     "leiningen"
     ;; "maven"
     ;; "openjdk"
     "openjdk@11.:jdk"
     ;; "openjdk:doc"
     "python"
     "python-pip"
     "python-virtualenv"
     "python-language-server"
   ))
