;; -*- mode: guix-scheme -*-

(use-modules
 (lp suckless))


(specifications->manifest
 '(
   "my-dmenu"
   "my-dwm"
   "my-dwmblocks"
   ))
;; (packages->manifest
;;  `((,my-dmenu "out"))
;;  `((,my-dwm "out"))
;;  `((,my-dwmblocks "out"))
;;  )
