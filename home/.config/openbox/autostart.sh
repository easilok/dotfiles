numlockx &
setxkbmap -option caps:escape &
xsetroot -cursor_name left_ptr &
nitrogen --restore &
xss-lock -- slock &
# xscreensaver -no-splash &
wmname compiz

lxpolkit &
polybar openbox &
picom &
sxhkd & 
~/scripts/set_wallpaper &
xfce4-clipman &
dunst &
xsettingsd &
