-- evil = {}

require("evil.volume")
require("evil.cpu")
require("evil.temperature")
require("evil.ram")
require("evil.mpd")
require("evil.battery")
require("evil.battery")
require("evil.archupdates")

-- return evil
